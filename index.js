console.log("Hello World"); 


let trainer = {};



trainer.name = 'Ash Ketchum';
trainer.age = 10;
trainer.pokemon = ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'];
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
}


trainer.talk = function() {
	console.log('Pikachu! I choose you!');
}

console.log(trainer);


trainer.talk();


function Pokemon(name, level) {
	//Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;


	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`)


		target.health -= this.attack;

		console.log(`${target.name}'s health is now reduced to ${target.health}`)

		if(target.health <= 0) {

			target.faint()
		}

	}

	this.faint = function() {
		console.log( this.name + ' fainted.')
	}

}

let pikachu = new Pokemon('Pikachu', 15);
console.log(pikachu);


let togepi = new Pokemon('Togepi', 8);
console.log(togepi);


let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);


togepi.tackle(pikachu);
pikachu.tackle(togepi);

mewtwo.tackle(pikachu);




